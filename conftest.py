import os

import pytest


@pytest.fixture(scope='session', autouse=True)
def env():
    yield
    with open(f"{os.getenv('PWD')}/some.json", 'r') as f:
        print(f.read())